# NPM Project Setup

Main goal of this project is to recommend tools and configurations that will make **you and your team happy** and **removes useless work**.

This folder contains recommended **linting**, **code-formatting**, and **scripts** setup for JavaScript/TypeScript projects.

Primary focus is on **React** (for now), but most of the configuration here is **also valid for non-React projects**.

## What is included?

- ESLint Plugins and Configuration
- Prettier
- EditorConfig
- NPM Scripts

## ESLint Plugins and Configuration

### What is ESLint and Why Should We Use It?

Lint is utility that will help you analyze code and find issues that could lead to potential problems (such as missing important attributes or not following [Rules of Hooks](https://reactjs.org/docs/hooks-rules.html)).

There were many JS lint tools, but today ESLint is the clear winner. Nice feature of ESLint is that it can also automatically fix the a lot of found issues.

Configuration is in `.eslintrc.js` file. ESLint configuration can be stored in many formats, but we recommend `.js` file, because it allows native usage of comments.

### ✅ ESLint Recommended ✅

There were many approach about how to configure ESLint, but current trend followed by big names in React is to have minimal and useful configuration, with focus on auto-fixing.

Dan Abramov and Kent C. Dodds both agree, that strict rules and overuse of ESLint are outdated ideas:

> Linters are there to help you find mistakes in your code. Not to torture you. If your linter tortures you, it’s not your fault (but might be your team lead’s).
>
> “Does this rule <s>bring me joy</s> help me find bugs?” If not, TURN IT OFF.
>
> — [Dan Abramov](https://twitter.com/dan_abramov/status/1086215771401920512) (also mentioned in [Marie Kondo Your Lint Config](https://overreacted.io/writing-resilient-components/#marie-kondo-your-lint-config))

> Delete `eslint-config-react` and `eslint-config-airbnb` and use `eslint-config-react-app` instead. Also... Use [prettier](https://prettier.io/)...
>
> — [Kent C. Dodds](https://kentcdodds.com/blog/why-users-care-about-how-you-write-code)

Usage of sensible preset is recommended:

- for Next.js: [`eslint-config-next`](https://www.npmjs.com/package/eslint-config-next) (this is standard part of Next.js)
- for non-Next.js React apps: [`eslint-config-react-app`](https://www.npmjs.com/package/eslint-config-react-app)

In addition, we find following plugin for sorting imports (with auto-fix support) very useful:

- [`eslint-plugin-simple-import-sort`](https://github.com/lydell/eslint-plugin-simple-import-sort)

### ❌ ESLint Not Recommended ❌

Following configurations used to be popular, but now they are considered outdated:

- Any complex setup containing many project specific customizations
  - It's hard to manage, upgrade or remove outdated rules.
  - Use of sensible preset (like `eslint-config-react-app`) is suggested.
- [`eslint-config-airbnb`](https://www.npmjs.com/package/eslint-config-airbnb)
  - This preset is very strict and sometimes more annoying to work with. You should always make your rules as easy as possible and not overhaul yourself with hundreds of rules that could hinder your developer flow. Also a lot of rules from this configuration are handled by Prettier.
- [`@typescript-eslint/recommended`](https://www.npmjs.com/package/@typescript-eslint/eslint-plugin)
  - This configuration contains rules like [`no-empty-interface`](https://github.com/typescript-eslint/typescript-eslint/blob/v5.9.1/packages/eslint-plugin/docs/rules/no-empty-interface.md) and [`no-inferrable-types`](https://github.com/typescript-eslint/typescript-eslint/blob/v5.9.1/packages/eslint-plugin/docs/rules/no-inferrable-types.md). Both of those rules will make your codebase worse in sense of readability and future refactorings.

### Install `eslint-config-react-app` for ESLint v7

This is relevant for Create React App v4, or any React project that can't use ESLint v8 (except of Next.js, where you should use [`eslint-config-next`](https://www.npmjs.com/package/eslint-config-next)).

```bash
yarn add --dev "eslint-config-react-app@^6.0.0"
```

`eslint-config-react-app` v6 requires following peer dependencies:

```bash
yarn add --dev "eslint@^7.5.0" "babel-eslint@^10.0.0" "@typescript-eslint/eslint-plugin@^4.0.0" "@typescript-eslint/parser@^4.0.0" "eslint-plugin-flowtype@^5.2.0" "eslint-plugin-import@^2.22.0" "eslint-plugin-jest@^24.0.0" "eslint-plugin-jsx-a11y@^6.3.1" "eslint-plugin-react@^7.20.3" "eslint-plugin-react-hooks@^4.0.8" "eslint-plugin-testing-library@^3.9.0"
```

### Install `eslint-config-react-app` for ESLint v8

This is relevant any React project that can use ESLint v8 (like Create React App v5).

```bash
yarn add --dev "eslint-config-react-app@^7.0.0"
```

`eslint-config-react-app` v7 requires only `eslint` as a peer dependency:

```bash
yarn add --dev "eslint@^8.0.0"
```

### Install `eslint-plugin-simple-import-sort` for ESLint v7/v8

There are few plugins for sorting imports, but [`eslint-plugin-simple-import-sort`](https://github.com/lydell/eslint-plugin-simple-import-sort) is definitely the best now for many reasons:

- it's auto-fixable
- divides imports into groups (NPM, project, style, ... imports)
- sorts imports and exports based on import paths
- sorting of named imports (`import { b, c, a } from 'x'` will be sorted to `import { a, b, c } from 'x'`).

Install:

```bash
yarn add --dev "eslint-plugin-simple-import-sort@^7.0.0" "eslint-plugin-import@^2.22.0"
```

Following configuration may be used to customize `eslint-plugin-simple-import-sort` for your project needs:

```json
{
  "plugins": ["simple-import-sort", "import"],
  "rules": {
    "simple-import-sort/imports": [
      "error",
      {
        "groups": [
          // Side effect imports.
          ["^\\u0000"],
          // Packages.
          // Things that start with a letter (or digit or underscore), or `@` followed by a letter.
          ["^(react|react-dom)(/.*|$)", "^(next)(/.*|$)", "^@?\\w"],
          // Internal packages.
          ["^(src)(/.*|$)"],
          // Absolute imports and other imports such as Vue-style `@/foo`.
          // Anything not matched in another group.
          ["^"],
          // Parent imports. Put `..` last.
          ["^\\.\\.(?!/?$)", "^\\.\\./?$"],
          // Other relative imports. Put same-folder imports and `.` last.
          ["^\\./(?=.*/)(?!/?$)", "^\\.(?!/?$)", "^\\./?$"],
          // Style imports.
          ["^.+\\.s?css$"]
        ]
      }
    ],
    "simple-import-sort/exports": "error",
    "import/first": "error",
    "import/newline-after-import": "error",
    "import/no-duplicates": "error"
  }
}
```

- `"groups"` configuration specifies hot to group different kinds of imports, based on their import path. Each group is separated with blank line.
  - `["^\\u0000"]` is group for side-effect imports
  - `["^(react|react-dom)(/.*|$)", "^(next)(/.*|$)", "^@?\\w"]` is group for NPM libraries. Each item in group array specifies which import paths will be ordered first in this group (`react/*` `react-dom/*` will be first), then import of `next/*`, and then rest of NPM libraries
    - you may customize this part, based on packages you use
  - `["^(src)(/.*|$)"],` is group for absolute imports in your project (e.g. `import { Button } from 'src/ui/atoms')`, or in monorepo setup (e.g. `import { Button } from '@cngroup/ui/atoms'`).
    - you may want to change it to something like `["^(@cngroup)(/.*|$)"],`
  - rest of rules groups are for relative imports `../*` and `./*`
  - as final group you have CSS imports
    - this may be modified for example if you use `.less`

### Prettier

Have you ever struggle with code-formatting and code style consistency in the project? Don't worry Prettier will do all of this for you. It is an opinionated utility for code-formatting that is easily integrable into most editors.

Configuration file is in the project root `.prettierrc.yaml`.

Preset rules are very simple:

- You should always use in the JS/TS project trailing commas. Your team will benefit from [**clean diffs & easier code-manipulations**](https://medium.com/@nikgraf/why-you-should-enforce-dangling-commas-for-multiline-statements-d034c98e36f8).
- Single quote is preferred in most JS/TS code, but you may want to change it.
- You should also avoid increasing of `printWidth` since it might be different from device to device, and you might accidentally prevent some developers fitting the code to their screen.

### EditorConfig

[EditorConfig](https://editorconfig.org/) is a simple plugin for many IDEs/editors that make project settings consistent.

For example if two developers working with different editors, EditorConfig will set up their configuration, so it uses the same basics. This includes encoding (utf-8), using spaces instead of tabs or adding a line at the end of the file. This is set to support Vim and other editors that automatically put line at the end of the file. There is a special rule for `.md` files to prevent removing trailing spaces (since they are significant in Markdown files).

Configuration file is in the root `.editorconfig`.

### NPM scripts

This setup also includes NPM scripts that will help you quickly run any of these utilities. Since we are using `yarn`, you can run a command `yarn <xxx>` to execute the script. If you are using `npm`, please run `npm run <xxx>` instead. All of those commands are in the `package.json` file in the `scripts` property.

- `ci` - Run lint and prettier in the CI/CD environment that prevents accidental change of code.
- `ci:lint` - Run lint in CI/CD environment without auto fix.
- `ci:prettier` - Run code format in CI/CD that will list all changes without auto fix.
- `ci:test` - Run test suites.
- `lint` - Run linting for local development.
- `lint:fix` - Run linting and auto fix for local development.
- `lint:watch` - Run watcher for linting. So it autorun when you save a file.
- `prettier` - Run code format with autofix.
- `prettier:base` - Contains common Prettier configuration used by `prettier` and `ci:prettier` scripts.
- `format` - Run `lint:fix` and then `prettier`.

#### What are `run-s` and `run-p`?

Those are shortcuts for running multiple command in sequence or parallel. For example `npm run lint && npm run prettier` can be shorten to `run-s lint prettier`. If you want to use parallel approach (it means commands will run at the same time) use `run-p` instead.

To support these shortcuts, there is a package called `npm-run-all`.
